import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {CV, CvComponent} from './cv/cv.component';
import {CvListComponent} from './cv-list/cv-list.component';
import {CvService} from './cv.service';
import {SideNavComponent} from './side-nav/side-nav.component'
import {SideNavService} from "./side-nav/side-nav.service";
import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import { CvFormComponent } from './cv-form/cv-form.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { CvStylingComponent } from './cv-styling/cv-styling.component';
import {MatIconModule, MatTooltipModule} from "@angular/material";
import 'hammerjs';

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'cvlist', component: CvListComponent},
  {path: 'createcv', component: CvFormComponent},
  {path: 'cvstyling/:id', component: CvStylingComponent},
  {path: '', redirectTo:'home', pathMatch:'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CvComponent,
    CvListComponent,
    SideNavComponent,
    CvFormComponent,
    TopBarComponent,
    CvStylingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatSidenavModule,
    MatTooltipModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {useHash: true}
    )
  ],
  providers: [CvService, SideNavService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
