import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  static helloMessage;

  constructor() {
    HomeComponent.helloMessage = "Witaj na stronie CVProject!";
  }

  ngOnInit() {

  }

  get helloMessage(){
    return HomeComponent.helloMessage;
  }


}

