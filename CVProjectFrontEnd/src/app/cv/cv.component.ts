import {Component, Input, OnInit, Output, ViewChildren} from '@angular/core';
import {CvService} from "../cv.service";

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit {
  @Input() cv: CV;
    constructor(private cvService: CvService) {

  }


  ngOnInit() {
  }



}

export class CV {
  id: string;
  title: string;
  name: string;
  hobbys: string;
  experience: string;
  skills: string;
  education: string;
  completed: boolean;
  createdAt: Date;
}
