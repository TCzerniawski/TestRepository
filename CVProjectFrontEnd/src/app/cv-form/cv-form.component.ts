import {Component, OnInit} from '@angular/core';
import {CV} from "../cv/cv.component";
import {CvService} from "../cv.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-cv-form',
  templateUrl: './cv-form.component.html',
  styleUrls: ['./cv-form.component.scss']
})
export class CvFormComponent implements OnInit {

  newCV: CV = new CV();

  constructor(private cvService: CvService, private router: Router) {
  }

  ngOnInit() {
  }


  createCV(cvForm: CV): void {
    this.cvService.createCV(cvForm);

  }

  submitted = false;

  onSubmit(cvForm: CV): void {
    this.submitted = true;
    this.createCV(cvForm);
  }
}
