import {Component, OnInit, ViewChildren} from '@angular/core';
import {CV} from "../cv/cv.component";
import {CvService} from "../cv.service";


@Component({
  selector: 'app-cv-list',
  templateUrl: './cv-list.component.html',
  styleUrls: ['./cv-list.component.scss']
})


export class CvListComponent implements OnInit {

  constructor(private cvService:CvService) {
  }

  cvs: CV[];
  ngOnInit(): void {
    this.getCVs();
  }



  getCVs(): void {
    this.cvService.getCvs().subscribe(res => {this.cvs = res as CV[]; console.log(this.cvs);});
  }



  deleteCV(id: string): void {
    this.cvService.deleteCV(id).subscribe(res=> {
      this.getCVs();
      console.log(res)
    });
  }

  updateCV(cvData: CV): void {

  }

  toggleCompleted(cvData: CV): void {

  }

  editCV(cvData: CV): void {
  }

  clearEditing(): void {

  }
}

