import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {CV} from "../cv/cv.component";
import {ActivatedRoute} from '@angular/router';
import {CvService} from "../cv.service";

@Component({
  selector: 'app-cv-styling',
  templateUrl: './cv-styling.component.html',
  styleUrls: ['./cv-styling.component.scss']
})
export class CvStylingComponent implements OnInit {

  cv: CV;
  cvdata: string[];
  cvSectionNames: string[] = ['Experience', 'Skills', 'Education', 'Hobbys'];
  id: string;
  stylingSet: boolean = false;
  @HostBinding('class')@Input('class') stylingClass: string;

  constructor(private route: ActivatedRoute, private cvService: CvService) {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit() {
    this.getCV(this.id);
    console.log(this.cv);

  }


  getCV(id: string) {
    this.cvService.getCv(id).subscribe(res => {
      console.log(res);
      this.cv = res as CV;
      setTimeout(() => {
        this.cvdata = [this.cv.experience, this.cv.skills, this.cv.education, this.cv.hobbys];
        this.CountSections(this.cvdata);
      }, 250);

    });
  }

  SetStyle(num: number) {
    switch (num) {
      case(1):
        this.stylingClass = "smaller-column";
        break;
      case(2):
        this.stylingClass = "equal-columns";
        break;
      case(3):
        this.stylingClass = "no-columns";
        break;
    }
    this.stylingSet = true;

  }

  public CountSections(data: string[]): Array<boolean> {

    let emptySections = [];

    for (let section of data) {
      if (section == "" || !section) {
        emptySections.push(true);
        console.log("Puste!")
      }
      else {
        emptySections.push(false);
        console.log("Cos jest!")
      }
    }
    console.log(emptySections);
    return emptySections;

  }
}
