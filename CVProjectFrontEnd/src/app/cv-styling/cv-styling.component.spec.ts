import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CvStylingComponent } from './cv-styling.component';

describe('CvStylingComponent', () => {
  let component: CvStylingComponent;
  let fixture: ComponentFixture<CvStylingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CvStylingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CvStylingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
