import {Injectable} from '@angular/core';

import 'rxjs/add/operator/toPromise';
import {CV} from "./cv/cv.component";
import {HttpClient} from "@angular/common/http";
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";
import {Router} from "@angular/router";


@Injectable()
export class CvService {
  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient,private router: Router) {
  }

  getCv(id: string){
    return this.http.get(this.baseUrl + '/api/cv' + '/'+ id).pipe(data => data);
  }

  getCvs(){
    return this.http.get<CV[]>(this.baseUrl + '/api/cv/');
  }

  createCV(cvData: CV){
    return this.http.post(this.baseUrl + '/api/cv/', cvData).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/','cvlist']);
      },
      err => {
        console.log("Error occured");
      }
    );
  }

  updateCV(cvData: CV):  Observable<any> {
    return this.http.put(this.baseUrl + '/api/cv/' + cvData.id, cvData);
  }

  deleteCV(id: string): Observable<any> {
    return this.http.delete(this.baseUrl + '/api/cv/' + id);
  }

  private handleError(error: any): Promise<any> {
    console.error('Some error occured', error);
    return Promise.reject(error.message || error);
  }
}
