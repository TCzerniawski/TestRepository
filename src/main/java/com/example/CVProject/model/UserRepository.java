package com.example.CVProject.model;

import com.example.CVProject.model.domain.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("cvProject")
public interface UserRepository extends MongoRepository<User, String> {

}
