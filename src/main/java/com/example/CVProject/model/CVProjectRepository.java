package com.example.CVProject.model;

import com.example.CVProject.model.domain.CVProject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
@Qualifier("cvProject")
public interface CVProjectRepository extends MongoRepository<CVProject, String> {


}
