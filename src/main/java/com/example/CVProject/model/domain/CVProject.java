package com.example.CVProject.model.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
@Getter
@Setter
@Document(collection = "CVProjects")
public class CVProject {
    @Id
    public String id;

    @NotBlank
    @Size(max = 100)
    @Indexed(unique = true)
    public String title;
    public String name;
    public String hobbys;
    public String experience;
    public String skills;
    public String education;

    public Boolean completed = false;

    public Date createdAt = new Date();

    private CVProject() {
    }

    public CVProject(String title, String name, String hobbys, String experience, String skills, String education, Boolean completed, Date createdAt) {
        this.title = title;
        this.name = name;
        this.hobbys = hobbys;
        this.experience = experience;
        this.skills = skills;
        this.education = education;
        this.completed = completed;
        this.createdAt = createdAt;
    }

    public CVProject findAll(String id) {
        return new CVProject();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CVProject cvProject = (CVProject) o;
        return Objects.equals(id, cvProject.id) &&
                Objects.equals(title, cvProject.title) &&
                Objects.equals(name, cvProject.name) &&
                Objects.equals(hobbys, cvProject.hobbys) &&
                Objects.equals(experience, cvProject.experience) &&
                Objects.equals(skills, cvProject.skills) &&
                Objects.equals(education, cvProject.education) &&
                Objects.equals(completed, cvProject.completed) &&
                Objects.equals(createdAt, cvProject.createdAt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, name, hobbys, experience, skills, education, completed, createdAt);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHobbys() {
        return hobbys;
    }

    public void setHobbys(String hobbys) {
        this.hobbys = hobbys;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return String.format(
                "CVProject[id=%s, title='%s', name='%s', hobbys='%s', experience='%s', skills='%s', education='%s', completed='%s', createdAt='%s']",
                id, title, name, hobbys, experience, skills, education, completed, createdAt);
    }
}

