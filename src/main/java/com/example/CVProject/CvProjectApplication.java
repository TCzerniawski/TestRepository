package com.example.CVProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication()
public class CvProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(CvProjectApplication.class, args);
    }

    @RequestMapping("/app/**")
    public String index() {
        return "forward:/index.html";
    }
//    @Autowired
//    CVProjectRepository cvProjectRepository;

//	@Bean
//	@ConfigurationProperties(prefix = "spring.mongodb")
//	public DataSource simonDataSource() {
//		return DataSourceBuilder.create().build();
//	}
}
