package com.example.CVProject.service;

import com.example.CVProject.model.UserRepository;
import com.example.CVProject.model.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/user/api")
public class UserService {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/user/{id}")
    public Optional<User> getCV(@PathVariable("Id") String id) {
        return userRepository.findById(id);
    }

    @PostMapping("/user")
    public String createUser(@Valid @RequestBody User user) {
        userRepository.save(user);
        return "registration success!";
    }
}
