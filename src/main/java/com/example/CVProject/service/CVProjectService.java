package com.example.CVProject.service;

import com.example.CVProject.model.CVProjectRepository;
import com.example.CVProject.model.domain.CVProject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CVProjectService {
    @Autowired
    CVProjectRepository cvProjectRepository;

    @GetMapping("/cv")
    public List<CVProject> getAllCVs(HttpServletResponse response) {
        List<CVProject> projects = cvProjectRepository.findAll();
        Integer numberOfProjects = projects.size();
        return cvProjectRepository.findAll();
    }

    @GetMapping("/cv/{id}")
    public Optional<CVProject> getCV(@PathVariable("id") String id) {
        return cvProjectRepository.findById(id);
    }

    @PostMapping("/cv")
    public void createCV(@Valid @RequestBody CVProject cv) {
        cvProjectRepository.save(cv);
    }


    @PutMapping(value = "/cv/{id}")
    public void updateCV(@PathVariable("id") String id,
                         @Valid @RequestBody CVProject cv) {
        cv.setTitle(cv.getTitle());
        cvProjectRepository.insert(cv);
    }

    @DeleteMapping(value = "/cv/{id}")
    public void deleteCV(@PathVariable("id") String id) {
        cvProjectRepository.delete(this.getCV(id).get());
    }


}
